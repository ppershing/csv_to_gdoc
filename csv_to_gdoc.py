#!/usr/bin/python

import sys
import argparse
import gspread
import getpass
import csv

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=argparse.FileType("r"), default=None)
    parser.add_argument("--user")
    parser.add_argument("--document")
    parser.add_argument("--sheet")
    parser.add_argument("--range")
    parser.add_argument("--delimiter", default="\t")
    FLAGS = parser.parse_args()
    print "Using '%s' as CSV delimiter" % FLAGS.delimiter
    if not FLAGS.input:
        if sys.stdin.isatty():
           raise RuntimeError("No input CSV, use either --input or pipe")
        FLAGS.input = sys.stdin
        sys.stdin = open("/dev/tty") # get konsole

    if not FLAGS.user:
        FLAGS.user = raw_input("Google username:")
    pwd = getpass.getpass("Google password:")
    g = gspread.login(FLAGS.user, pwd)

    if not FLAGS.document:
        FLAGS.document = raw_input("Document name:")

    doc = g.open(FLAGS.document)

    if not FLAGS.sheet:
        FLAGS.sheet = raw_input("Sheet name")
    sheet = doc.worksheet(FLAGS.sheet)
    
    if not FLAGS.range:
        FLAGS.range = raw_input("Cell range (e.g. A1:B100)")

    cell_list = sheet.range(FLAGS.range)
    start_col = cell_list[0].col
    start_row = cell_list[0].row
    
    def find_cell(cell_list, row, col):
        for cell in cell_list:
            if cell.row == row and cell.col == col:
                return cell
        raise RuntimeError(
            "Problem finding cell (col=%d,row=%d). "
            "Did you specify big enough range?"
            % (col,row)
        )

    print "Parsing CSV..."
    for row, data in enumerate(csv.reader(FLAGS.input, delimiter=FLAGS.delimiter), start=start_row):
        for col, value in enumerate(data, start=start_col):
            find_cell(cell_list, row, col).value = value
    print "Updating..."
    sheet.update_cells(cell_list)
    print "All done"
