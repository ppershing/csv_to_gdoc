# CSV to Google docs (spreadsheets)
An utility which uploads a CSV into given gdoc.

Example usage:
```
./csv_to_gdoc.py --user=me@gmail.com --document="Document Name" --sheet=Sheet1 --range=B3:C4 --delimiter=, < myfile.csv
```

Note: If you encounter 403 error during authentication, change your settings in
https://www.google.com/settings/security/lesssecureapps
